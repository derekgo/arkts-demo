export interface TitleBarAttribute {
  // 返回键属性设置
  backShow?: boolean; // 是否显示返回键
  backCallback?: Function;  // 点击返回键的事件回调

  // 标题属性设置
  title: string;        // 设置标题文本

  // 菜单属性设置
  rightBtnShow?: boolean; // 时候显示菜单
  rightBtnCallback?: Function;  // 点击菜单键的事件回调
}